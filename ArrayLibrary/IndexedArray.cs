﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayLibrary
{
    public class IndexedArray : IComparable<IndexedArray>
    {
        private double[] _vector;
        public int Length { get; private set; }
        public int FirstIndex { get; private set; }

        /// <summary>
        /// Constructor taking initial index and length of the vector
        /// </summary>
        /// <param name="firstIndex">Initial index</param>
        /// <param name="length">Length of the vector</param>
        public IndexedArray(int firstIndex=0, int length=0)
        {
            FirstIndex = firstIndex;
            Length = length;
            _vector = new double[Length];
        }

        /// <summary>
        /// Constructor taking array of double values and initial index
        /// </summary>
        /// <param name="array">Array of double values</param>
        /// <param name="firstIndex">Initial index</param>
        public IndexedArray(double[] array, int firstIndex=0)
        {
            _vector = array;
            FirstIndex = firstIndex;
            Length = array.Length;
        }



        /// <summary>
        /// Indexator
        /// </summary>
        /// <param name="index">Index</param>
        /// <returns>Double value at the index position</returns>
        public double this[int index]
        {
            get
            {
                if (index < FirstIndex || index > (FirstIndex + Length))
                    throw new IndexOutOfRangeException();
                else
                    return _vector[index - FirstIndex];
            }
            set
            {
                if (index < FirstIndex || index > (FirstIndex + Length))
                    throw new IndexOutOfRangeException();
                else
                    _vector[index - FirstIndex] = value;
            }
        }

        /// <summary>
        /// Overloaded operator for addition two IndexedArray with same length and initial index
        /// </summary>
        /// <param name="arr1">First IndexedArray</param>
        /// <param name="arr2">Second IndexedArray</param>
        /// <returns>New IndexedArray</returns>
        public static IndexedArray operator+(IndexedArray arr1, IndexedArray arr2)
        {
            if (arr1 is null || arr2 is null)
                throw new ArgumentNullException("Can not add null arrays");
            if (arr1.FirstIndex != arr2.FirstIndex || arr1.Length != arr2.Length)
                throw new ArgumentException("This arrays have different index intervals");
            IndexedArray resultArr = new IndexedArray(arr1.FirstIndex, arr1.Length);
            for(int i = arr1.FirstIndex; i < arr1.FirstIndex + arr1.Length; i++)
            {
                resultArr[i] = arr1[i] + arr2[i];
            }
            return resultArr;
        }

        /// <summary>
        /// Overloaded operator for subtraction two IndexedArray with same length and initial index
        /// </summary>
        /// <param name="arr1">First IndexedArray</param>
        /// <param name="arr2">Second IndexedArray</param>
        /// <returns>New IndexedArray</returns>
        public static IndexedArray operator -(IndexedArray arr1, IndexedArray arr2)
        {
            if (arr1 is null || arr2 is null)
                throw new ArgumentNullException("Can not subtract null arrays");
            if (arr1.FirstIndex != arr2.FirstIndex || arr1.Length != arr2.Length)
                throw new ArgumentException("This arrays have different index intervals");
            IndexedArray resultArr = new IndexedArray(arr1.FirstIndex, arr1.Length);
            for (int i = arr1.FirstIndex; i < arr1.FirstIndex + arr1.Length; i++)
            {
                resultArr[i] = arr1[i] - arr2[i];
            }
            return resultArr;
        }

        /// <summary>
        /// Overloaded operator for multiplying IndexedArray and double number
        /// </summary>
        /// <param name="arr">IndexedArray</param>
        /// <param name="number">Double value</param>
        /// <returns>New IndexedArray</returns>
        public static IndexedArray operator*(IndexedArray arr, double number)
        {
            if (arr is null)
                throw new ArgumentNullException("Can not multiply null array");
            IndexedArray resultArr = new IndexedArray(arr.FirstIndex, arr.Length);
            for(int i = arr.FirstIndex; i < arr.FirstIndex + arr.Length; i++)
            {
                resultArr[i] = arr[i] * number;
            }
            return resultArr;
        }

        /// <summary>
        /// Overloaded operator for multiplying double number and IndexedArray
        /// </summary>
        /// <param name="number">Double number</param>
        /// <param name="arr">IndexedArray</param>
        /// <returns>New IndexedArray</returns>
        public static IndexedArray operator*(double number, IndexedArray arr)
        {
            return arr * number;
        }

        /// <summary>
        /// Overrided function for comparing two IndexedArrays
        /// </summary>
        /// <param name="obj">Object</param>
        /// <returns>True, if objects are equal</returns>
        public override bool Equals(object obj)
        {
            IndexedArray array = obj as IndexedArray;
            if (array is null)
                throw new ArgumentException("Given object is not a IndexedArray");
            if (this.FirstIndex != array.FirstIndex || this.Length != array.Length)
                return false;
            for(int i=this.FirstIndex; i < this.FirstIndex + this.Length; i++)
            {
                if (this[i] != array[i])
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Overloaded operator for comparing two IndexedArray objects
        /// </summary>
        /// <param name="arr1">First IndexedArray object</param>
        /// <param name="arr2">Second IndexedArray object</param>
        /// <returns>True, if objects are equal</returns>
        public static bool operator==(IndexedArray arr1, IndexedArray arr2)
        {
            if (arr1 is null)
                throw new ArgumentNullException("Can not compare null array");
            return arr1.Equals(arr2);
        }

        /// <summary>
        /// Overloaded operator for comparing two IndexedArray objects
        /// </summary>
        /// <param name="arr1">First IndexedArray object</param>
        /// <param name="arr2">Second IndexedArray object</param>
        /// <returns>True, if objects are not equal</returns>
        public static bool operator !=(IndexedArray arr1, IndexedArray arr2)
        {
            if (arr1 is null)
                throw new ArgumentNullException("Can not compare null array");
            return !arr1.Equals(arr2);
        }

        /// <summary>
        /// Function for comparing this IndexedArray object with another IndexedArray object
        /// </summary>
        /// <param name="arr">IndexedArray</param>
        /// <returns>Int value</returns>
        public int CompareTo(IndexedArray arr)
        {
            if (arr is null)
                throw new ArgumentNullException("Can not compare null array");
            if (this.Length != arr.Length)
                throw new ArgumentException("This arrays have different sizes");

            int j = arr.FirstIndex;
            bool isGreater = true;
            for(int i = this.FirstIndex; i < this.FirstIndex + this.Length; i++)
            {
                if (this[i] < arr[j])
                    isGreater = false;
                j++;
            }
            if (isGreater)
                return 1;

            j = arr.FirstIndex;
            bool isSmaller = true;
            for (int i = this.FirstIndex; i < this.FirstIndex + this.Length; i++)
            {
                if (this[i] > arr[j])
                    isSmaller = false;
                j++;
            }
            if (isSmaller)
                return -1;

            return 0;
        }

        /// <summary>
        /// Overloaded operator for comparing two IndexedArray objects
        /// </summary>
        /// <param name="arr1">First IndexedArray object</param>
        /// <param name="arr2">Second indexedArray object</param>
        /// <returns>True, if First IndexedArray object is greater than Second indexedArray object
        /// False in another case</returns>
        public static bool operator>(IndexedArray arr1, IndexedArray arr2)
        {
            if (arr1 is null)
                throw new ArgumentNullException("Can not compare null array");
            return arr1.CompareTo(arr2) > 0;
        }

        /// <summary>
        /// Overloaded operator for comparing two IndexedArray objects
        /// </summary>
        /// <param name="arr1">First IndexedArray object</param>
        /// <param name="arr2">Second indexedArray object</param>
        /// <returns>True, if First IndexedArray object is smalles than Second indexedArray object
        /// False in another case</returns>
        public static bool operator <(IndexedArray arr1, IndexedArray arr2)
        {
            if (arr1 is null)
                throw new ArgumentNullException("Can not compare null array");
            return arr1.CompareTo(arr2) < 0;
        }

        /// <summary>
        /// Overloaded operator for comparing two IndexedArray objects
        /// </summary>
        /// <param name="arr1">First IndexedArray object</param>
        /// <param name="arr2">Second indexedArray object</param>
        /// <returns>True, if First IndexedArray object is greater than or equal to the Second IndexedArray object
        /// False in another case</returns>
        public static bool operator >=(IndexedArray arr1, IndexedArray arr2)
        {
            if (arr1 is null)
                throw new ArgumentNullException("Can not compare null array");
            return arr1.CompareTo(arr2) >= 0;
        }

        /// <summary>
        /// Overloaded operator for comparing two IndexedArray objects
        /// </summary>
        /// <param name="arr1">First IndexedArray object</param>
        /// <param name="arr2">Second indexedArray object</param>
        /// <returns>True, if First IndexedArray object is smaller or equal to the Second IndexedArray object
        /// False in another case</returns>
        public static bool operator <=(IndexedArray arr1, IndexedArray arr2)
        {
            if (arr1 is null)
                throw new ArgumentNullException("Can not compare null array");
            return arr1.CompareTo(arr2) <= 0;
        }

        /// <summary>
        /// Represents IndexedArray object as a string
        /// </summary>
        /// <returns>String value</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(this.Length);
            for(int i = this.FirstIndex; i < this.FirstIndex + this.Length; i++)
            {
                sb.Append(string.Format($"{this[i]} "));
            }
            return sb.ToString();
        }
    }
}
