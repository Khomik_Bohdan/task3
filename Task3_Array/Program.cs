﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArrayLibrary;

namespace Task3_Array
{
    class Program
    {
        static void Main(string[] args)
        {
            TestIndexedArrayLibrary();
        }

        public static void TestIndexedArrayLibrary()
        {
            IndexedArray arr1 = new IndexedArray(new double[5] { 1, 1, 1, 1, 1 }, -5);
            IndexedArray arr2 = new IndexedArray(new double[5] { 1, 2, 3, 4, 5 }, -5);
            IndexedArray arr3 = new IndexedArray(new double[5] { 3, 4, 5, 6, 7 });
            IndexedArray arr4 = new IndexedArray(new double[5] { 5, 6, 7, 8, 9 });
            Console.WriteLine($"IndexedArray1: {arr1}");
            Console.WriteLine($"IndexedArray2: {arr2}");
            Console.WriteLine($"Get element from the second IndexedArray at the index -3: {arr2[-3]}");
            Console.WriteLine($"IndexedArray2 + IndexedArray1: {arr1+arr2}");
            Console.WriteLine($"IndexedArray2 - IndexedArray1: {arr2 - arr1}");
            try
            {
                Console.WriteLine($"IndexedArray2 + IndexedArray3: {arr2 + arr3}");
            }
            catch(ArgumentException argEx)
            {
                Console.WriteLine("Adding a two IndexedArray with a different index bounds is not allowed");
            }
            Console.WriteLine($"IndexedArray1 * 5: {arr1*5}");
            Console.WriteLine($"Is arr1 equal to arr2: {arr1==arr2}");
            Console.WriteLine($"IndexedArray3: {arr3}");
            Console.WriteLine($"Is arr2 less than arr3: {arr2<arr3}");//they have differrent first index but same length
            Console.ReadKey();
        }
    }
}
